//
//  JsonRequestHelper.swift
//  PersonalityTest
//
//  Created by Alex Balan on 06.04.2022.
//

import Foundation

class JsonRequestHelper{

    // loading from local file
    func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name, ofType: "json"),

                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }

        return nil
    }

    // loading from url
    private func loadJson(fromURLString urlString: String, completion: @escaping (Result<Data, Error>) -> Void) {

        if let url = URL(string: urlString) {

            let urlSession = URLSession(configuration: .default).dataTask(with: url) { (data, response, error) in
                if let error = error {
                    completion(.failure(error))
                }

                if let data = data {
                    completion(.success(data))
                }
            }

            urlSession.resume()
        }
    }

}
