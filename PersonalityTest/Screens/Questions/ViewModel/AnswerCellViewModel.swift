//
//  AnswerCellViewModel.swift
//  PersonalityTest
//
//  Created by Alex Balan on 06.04.2022.
//

import Foundation


struct AnswerCellViewModel{

    var letter: String
    var answer: String
    var points: Int
    
}
