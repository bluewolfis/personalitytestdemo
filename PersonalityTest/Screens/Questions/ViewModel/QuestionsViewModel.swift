//
//  QuestionsViewModel.swift
//  PersonalityTest
//
//  Created by Alex Balan on 06.04.2022.
//

import Foundation

class QuesionsViewModel: NSObject{

    private var questionService:QuestionsServiceProtocol

    var questionText = Dynamic("")
    var buttonText = Dynamic("")

    var reloadTableView: (() -> Void)?

    var questions = Questions()

    var currentQuestion:Question? = nil

    var currentQuestionIndex = 0

    var score = 0

    var showResults:Bool = false

    var answerCellViewModels = [AnswerCellViewModel]() {
        didSet {
            reloadTableView?()
        }
    }

    init(questionService: QuestionsServiceProtocol = QuestionsService()) {
        self.questionService = questionService
    }

    func getQuestions() {
        questionService.getQuestions(completion: { success, model, error in

            if success, let questions = model {
                self.fetchData(questions: questions)
            } else {
                print(error!)
            }
        })
    }

    func fetchData(questions: Questions) {

        self.questions = questions // Cache

        displayQuestionAndAnswers()
    }

    func displayQuestionAndAnswers(){

        currentQuestion = questions[currentQuestionIndex]

        var vms = [AnswerCellViewModel]()

        for answer in currentQuestion!.answers{
            vms.append(createCellModel(answer: answer))
        }

        answerCellViewModels = vms

        let qn = currentQuestionIndex + 1
        let qc = questions.count

        let numquestion = "Question \(qn) / \(qc) \n\n"

        questionText.value =  numquestion + currentQuestion!.question

        if qn != qc {
            buttonText.value = "Next Question"
        }else{
            buttonText.value = "Finish"
        }
    }

    func diplayNextQuestion(){

        if currentQuestionIndex < questions.count - 1 {

            currentQuestionIndex += 1

            displayQuestionAndAnswers()

            showResults = false

        } else{

            showResults = true

        }

    }

    func updateScore(selectedanswer:Int){

        print(selectedanswer)

        score = score + (currentQuestion?.answers[selectedanswer].points ?? 0)

        print("Total Score:  \(score) ")
    }

    func createCellModel(answer: Answer) -> AnswerCellViewModel {

        return AnswerCellViewModel(letter: answer.letter, answer: answer.answer, points: answer.points)
    }

    func getCellViewModel(at indexPath: IndexPath) -> AnswerCellViewModel {
        return answerCellViewModels[indexPath.row]
    }

}
