//
//  Question.swift
//  PersonalityTest
//
//  Created by Alex Balan on 06.04.2022.
//

// good resource https://app.quicktype.io/


import Foundation

typealias Questions = [Question]


// MARK: - Question

struct Question: Codable {
    let question: String
    let answers: [Answer]

}

// MARK: - Answer

struct Answer: Codable {
    let letter: String
    let answer: String
    let points: Int

}
