//
//  AnswerCell.swift
//  PersonalityTest
//
//  Created by Alex Balan on 06.04.2022.
//

import UIKit

class AnswerCell: UITableViewCell {

    @IBOutlet weak var answerLetterLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!

    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }

    var points:Int? = nil

    var cellViewModel: AnswerCellViewModel? {
        didSet {

            answerLetterLabel.text = cellViewModel?.letter
            answerLabel.text = cellViewModel?.answer

            points = cellViewModel?.points

        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        answerLetterLabel.text = nil
        answerLabel.text = nil
    }

    
}
