//
//  ResultsViewController.swift
//  PersonalityTest
//
//  Created by Alex Balan on 07.04.2022.
//

import UIKit

class ResultsViewController: UIViewController {


    @IBOutlet weak var resultsLbl: UILabel!


    var score:Int = 0
    var numquestions:Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()


        print("Score \(score) ")
        print("Questions \(numquestions)")

        var introvertorextrovert = "Introvert"

        let result = Double(score) / Double(numquestions)

        print("Result \(result)")

        if result > 1.0 {
            introvertorextrovert = "Extrovert"
        }

        resultsLbl.text = "You obtained \(score) points from \(numquestions) questions so you are probabbly an \(introvertorextrovert)"
    }

    @IBAction func onClose(_ sender: Any) {

        dismiss(animated: true)
    }


}
