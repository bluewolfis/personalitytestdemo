//
//  MainViewController.swift
//  PersonalityTest
//
//  Created by Alex Balan on 06.04.2022.
//

import UIKit

class MainViewController: UIViewController {

    var score:Int = 0
    var numquestions:Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTest" {
            let destination = segue.destination as? QuestionsViewController
            destination?.origin = self
        }

        if segue.identifier == "showResult" {
            let destination = segue.destination as? ResultsViewController
            destination?.score = score
            destination?.numquestions = numquestions
        }
    }

    func displayResult(score:Int,numquestions:Int){

        self.score = score
        self.numquestions = numquestions

        performSegue(withIdentifier: "showResult", sender: self)
    }

}

