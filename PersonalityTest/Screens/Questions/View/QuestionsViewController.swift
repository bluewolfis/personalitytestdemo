//
//  QuestionsViewController.swift
//  PersonalityTest
//
//  Created by Alex Balan on 06.04.2022.
//

import UIKit

class QuestionsViewController: UIViewController{

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answersTableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!

    var origin:MainViewController? = nil

    var selectedrow = 0

    lazy var viewModel = {
        QuesionsViewModel()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        initViewModel()
    }

    func initView() {

        answersTableView.delegate = self
        answersTableView.dataSource = self

        answersTableView.register(AnswerCell.nib, forCellReuseIdentifier: AnswerCell.identifier)

        nextBtn.isEnabled = false
    }

    func initViewModel() {
        viewModel.getQuestions()

        viewModel.reloadTableView = { [weak self] in
            DispatchQueue.main.async {
                self?.answersTableView.reloadData()
            }
        }

        viewModel.questionText.bindAndFire { questionText in
            DispatchQueue.main.async {
                self.questionLabel.text = questionText
            }
        }

        viewModel.buttonText.bindAndFire { buttonText in
            DispatchQueue.main.async {
                self.nextBtn.setTitle(buttonText, for: .normal)
            }
        }
    }

    @IBAction func displayNextQuestionOrResult(_ sender: Any) {

        viewModel.updateScore(selectedanswer: selectedrow)
        viewModel.diplayNextQuestion()
        
        nextBtn.isEnabled = false

        if(viewModel.showResults){

            dismiss(animated: true) {
                self.origin?.displayResult(score: self.viewModel.score, numquestions: self.viewModel.questions.count)
            }

        }

    }
    

}

// MARK: - UITableViewDelegate

extension QuestionsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

// MARK: - UITableViewDataSource

extension QuestionsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return viewModel.answerCellViewModels.count

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: AnswerCell.identifier, for: indexPath) as? AnswerCell else { fatalError("xib does not exists") }
        let cellVM = viewModel.getCellViewModel(at: indexPath)
        cell.cellViewModel = cellVM
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nextBtn.isEnabled = true
        selectedrow = indexPath.row
    }
}
