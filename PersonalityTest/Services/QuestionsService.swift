//
//  QuestionsService.swift
//  PersonalityTest
//
//  Created by Alex Balan on 06.04.2022.
//

import Foundation

protocol QuestionsServiceProtocol {
    func getQuestions(completion: @escaping (_ success: Bool, _ results: Questions?, _ error: String?) -> ())
}

class QuestionsService: QuestionsServiceProtocol {

    func getQuestions(completion: @escaping (Bool, Questions?, String?) -> ()) {


        if let jsonData = JsonRequestHelper().readLocalFile(forName: "questions") {

            do {
                let model = try JSONDecoder().decode(Questions.self, from: jsonData)
                completion(true, model, nil)

            }catch {
                print(error)
                completion(false, nil, "Error: Trying to parse Questions to model")
            }


        } else {
            print("error loading json file")

            completion(false, nil, "error loading json file")
        }

    }
}
